use anyhow::{bail, Context, Result};
use chrono::{DateTime, Duration, Local};
use glob::glob;
use lazy_static::lazy_static;
use num_traits::{One, Zero};
use ordered_float::OrderedFloat;
use plotters::prelude::{
    BitMapBackend, ChartBuilder, Color, IntoDrawingArea, LineSeries,
    PathElement, SeriesLabelPosition, ShapeStyle, BLACK, BLUE, CYAN,
    GREEN, MAGENTA, RED, WHITE,
};
use regex::Regex;
use serde::{Deserialize, Serialize};
use std::collections::BTreeMap;
use std::io::Write;
use std::path::{Path, PathBuf};
use structopt::StructOpt;

fn default_chart_width() -> u32 {
    800
}

fn default_chart_height() -> u32 {
    600
}

#[derive(Serialize, Deserialize, Debug, Default)]
struct CurveFile {
    curves: ChartRawData,
}

#[derive(Serialize, Deserialize, Debug, Default)]
struct CurveFloatFile {
    curves: ChartData,
}

type CurveRawData = BTreeMap<DateTime<Local>, i16>;

type ChartRawData = BTreeMap<String, CurveRawData>;

trait CurveConvertToFloat {
    type Target;

    fn convert_to_float(&self) -> Self::Target;
}

impl CurveConvertToFloat for CurveRawData {
    type Target = CurveData;

    fn convert_to_float(&self) -> Self::Target {
        self.iter()
            .map(|(k, v)| (*k, OrderedFloat(*v as f32 * 0.001)))
            .collect()
    }
}

impl CurveConvertToFloat for ChartRawData {
    type Target = ChartData;

    fn convert_to_float(&self) -> Self::Target {
        self.iter()
            .map(|(k, v)| (k.to_string(), v.convert_to_float()))
            .collect()
    }
}

impl CurveConvertToFloat for CurveFile {
    type Target = CurveFloatFile;

    fn convert_to_float(&self) -> Self::Target {
        CurveFloatFile {
            curves: self.curves.convert_to_float(),
        }
    }
}

trait ExtractExtremes<T> {
    fn extract_min_max(&self) -> (T, T);
}

impl ExtractExtremes<i16> for CurveRawData {
    fn extract_min_max(&self) -> (i16, i16) {
        let mut min = 0;
        let mut max = 0;

        for v in self.values() {
            min = std::cmp::min(min, *v);
            max = std::cmp::max(max, *v);
        }
        (min, max)
    }
}

impl ExtractExtremes<i16> for ChartRawData {
    fn extract_min_max(&self) -> (i16, i16) {
        let mut min = 0;
        let mut max = 0;

        for v in self.values() {
            let (vmin, vmax) = v.extract_min_max();
            min = std::cmp::min(min, vmin);
            max = std::cmp::max(max, vmax);
        }
        (min, max)
    }
}

impl ExtractExtremes<OrderedFloat<f32>> for CurveData {
    fn extract_min_max(&self) -> (OrderedFloat<f32>, OrderedFloat<f32>) {
        let mut min = OrderedFloat::<f32>::zero();
        let mut max = OrderedFloat::<f32>::zero();

        for v in self.values() {
            min = std::cmp::min(min, *v);
            max = std::cmp::max(max, *v);
        }
        (min, max)
    }
}

impl ExtractExtremes<OrderedFloat<f32>> for ChartData {
    fn extract_min_max(&self) -> (OrderedFloat<f32>, OrderedFloat<f32>) {
        let mut min = OrderedFloat::<f32>::zero();
        let mut max = OrderedFloat::<f32>::zero();

        for v in self.values() {
            let (vmin, vmax) = v.extract_min_max();
            min = std::cmp::min(min, vmin);
            max = std::cmp::max(max, vmax);
        }
        (min, max)
    }
}

type CurveData = BTreeMap<DateTime<Local>, OrderedFloat<f32>>;

type ChartData = BTreeMap<String, CurveData>;

trait CurveFlatten {
    fn flattened(&self, alpha: OrderedFloat<f32>) -> Self;
}

impl CurveFlatten for CurveData {
    fn flattened(&self, alpha: OrderedFloat<f32>) -> Self {
        let mut prev: Option<OrderedFloat<f32>> = None;

        let mut flattened = BTreeMap::new();

        for (k, v) in self {
            match prev {
                None => prev = Some(*v),
                Some(p) => {
                    let current = (*v * alpha)
                        + (OrderedFloat::<f32>::one() - alpha) * p;
                    prev = Some(current);
                    flattened.insert(*k, current);
                }
            }
        }
        flattened
    }
}

impl CurveFlatten for ChartData {
    fn flattened(&self, alpha: OrderedFloat<f32>) -> Self {
        let mut flattened = BTreeMap::new();
        for (key, curve) in self {
            flattened.insert(key.to_string(), curve.flattened(alpha));
        }
        flattened
    }
}

impl CurveFlatten for CurveFloatFile {
    fn flattened(&self, alpha: OrderedFloat<f32>) -> Self {
        CurveFloatFile {
            curves: self.curves.flattened(alpha),
        }
    }
}

#[derive(Serialize, Deserialize, Debug, Clone)]
struct Configuration {
    #[serde(default = "default_chart_width")]
    chart_width: u32,
    #[serde(default = "default_chart_height")]
    chart_height: u32,

    mapping: BTreeMap<String, String>,
}

impl Default for Configuration {
    fn default() -> Self {
        Configuration {
            chart_width: default_chart_width(),
            chart_height: default_chart_height(),
            mapping: Default::default(),
        }
    }
}

#[derive(Debug, StructOpt)]
enum Command {
    /// Extract the data from the mesurement readings into a curve file
    Extract(ExtractArgs),

    /// Flatten a curve file
    Flatten(FlattenArgs),

    /// Generate a chart from prepared curve data
    Generate(GenerateArgs),
}

impl Command {
    fn process(self, config: &Configuration) -> Result<()> {
        match self {
            Self::Generate(args) => args.process(config),
            Self::Extract(args) => args.process(config),
            Self::Flatten(args) => args.process(config),
        }
    }
}

#[derive(Debug, StructOpt)]
struct Opt {
    #[structopt(long)]
    configfile: Option<PathBuf>,

    #[structopt(subcommand)]
    command: Command,
}

#[derive(Debug, StructOpt)]
struct ExtractArgs {
    /// The directory where the logged temperature files are placed
    indatadir: PathBuf,

    /// The file where the converted data is stored
    outfile: PathBuf,
}

impl ExtractArgs {
    fn process(self, config: &Configuration) -> Result<()> {
        let pattern = format!("{}/*", self.indatadir.as_str()?);

        let mut min = OrderedFloat::zero();
        let mut max = OrderedFloat::zero();

        let mut curvefile = if self.outfile.exists() {
            let data_str = std::fs::read_to_string(&self.outfile)?;
            let data: CurveFloatFile = toml::from_str(&data_str)?;
            data
        } else {
            CurveFloatFile::default()
        };

        for entry in glob(&pattern).with_context(|| {
            format!("Failed to read glob pattern {:?}", pattern)
        })? {
            match entry {
                Ok(path) => {
                    let filename_raw =
                        path.file_name().with_context(|| {
                            format!(
                                "Couldn't get directory name of {:?}",
                                path
                            )
                        })?;
                    let filename =
                        filename_raw.as_str().with_context(|| {
                            format!(
                                "Couldn't get directory name, possibly \
                            invalid UTF-8: {:?}",
                                filename_raw
                            )
                        })?;
                    if let Ok(dt) = filename.parse::<DateTime<Local>>() {
                        match load_point(&path) {
                            Ok(point) => {
                                for (k, v) in point {
                                    let mapped_k = config
                                        .mapping
                                        .get(&k)
                                        .unwrap_or(&k)
                                        .to_string();
                                    let curve = curvefile
                                        .curves
                                        .entry(mapped_k)
                                        .or_insert_with(BTreeMap::new);
                                    let float_v =
                                        OrderedFloat(v as f32 * 0.001);
                                    curve.insert(dt, float_v);
                                    min = std::cmp::min(min, float_v);
                                    max = std::cmp::max(max, float_v);
                                }
                            }
                            Err(e) => {
                                eprintln!(
                                    "Error loading point {}: {:?}",
                                    dt, e
                                );
                            }
                        }
                    }
                }
                Err(e) => eprintln!("{:?}", e),
            }
        }

        let toml_str = toml::to_string_pretty(&curvefile)?;
        let mut file = std::fs::File::create(&self.outfile)?;
        file.write_all(toml_str.as_bytes())?;

        Ok(())
    }
}

#[derive(Debug, StructOpt)]
struct FlattenArgs {
    /// The extracted input curve file.
    infile: PathBuf,

    /// The file where the converted data is stored
    outfile: PathBuf,
}

impl FlattenArgs {
    fn process(self, _config: &Configuration) -> Result<()> {
        let data_str = std::fs::read_to_string(self.infile)?;
        let data: CurveFloatFile = toml::from_str(&data_str)?;

        let toml_str = toml::to_string_pretty(
            &data.flattened(OrderedFloat::<f32>::from(0.3)),
        )?;
        let mut file = std::fs::File::create(self.outfile)?;
        file.write_all(toml_str.as_bytes())?;

        Ok(())
    }
}

#[derive(Debug, StructOpt)]
struct GenerateArgs {
    /// The converted file where the data is stored
    indatafile: PathBuf,

    /// The directory where the generated files are placed
    outdir: PathBuf,
}

impl GenerateArgs {
    fn process(self, config: &Configuration) -> Result<()> {
        let data_str = std::fs::read_to_string(self.indatafile)?;
        let data: CurveFloatFile = toml::from_str(&data_str)?;

        let curves = data
            .curves
            .into_iter()
            .map(|(k, v)| {
                (config.mapping.get(&k).unwrap_or(&k).to_string(), v)
            })
            .collect::<BTreeMap<_, _>>();
        let end = Local::now();
        {
            let start =
                end.checked_sub_signed(Duration::hours(1)).unwrap();
            write_chart(
                &self.outdir.join("hour.png"),
                config.chart_width,
                config.chart_height,
                start,
                end,
                &curves,
            )?;
        }
        {
            let start =
                end.checked_sub_signed(Duration::hours(3)).unwrap();
            write_chart(
                &self.outdir.join("three-hours.png"),
                config.chart_width,
                config.chart_height,
                start,
                end,
                &curves,
            )?;
        }
        {
            let start = end.checked_sub_signed(Duration::days(1)).unwrap();
            write_chart(
                &self.outdir.join("day.png"),
                config.chart_width,
                config.chart_height,
                start,
                end,
                &curves,
            )?;
        }
        {
            let start = end.checked_sub_signed(Duration::days(2)).unwrap();
            write_chart(
                &self.outdir.join("two-days.png"),
                config.chart_width,
                config.chart_height,
                start,
                end,
                &curves,
            )?;
        }
        {
            let start = end.checked_sub_signed(Duration::days(7)).unwrap();
            write_chart(
                &self.outdir.join("week.png"),
                config.chart_width,
                config.chart_height,
                start,
                end,
                &curves,
            )?;
        }
        {
            let start =
                end.checked_sub_signed(Duration::weeks(5)).unwrap();
            write_chart(
                &self.outdir.join("month.png"),
                config.chart_width,
                config.chart_height,
                start,
                end,
                &curves,
            )?;
        }
        {
            let start =
                end.checked_sub_signed(Duration::weeks(52)).unwrap();
            write_chart(
                &self.outdir.join("year.png"),
                config.chart_width,
                config.chart_height,
                start,
                end,
                &curves,
            )?;
        }
        Ok(())
    }
}

fn read_file(file: &Path) -> Result<i16> {
    lazy_static! {
        static ref RE: Regex = Regex::new(" t=(?P<value>[0-9]+)").unwrap();
    }
    let s = std::fs::read_to_string(file)?;

    match RE.captures(&s) {
        Some(captures) => {
            let raw = captures["value"].parse::<u32>()?;
            let mut bytes = raw.to_le_bytes();
            if bytes[2] != 0 {
                bytes[1] |= 0b1000_0000;
            }
            let value = i16::from_be_bytes([bytes[1], bytes[0]]);
            Ok(value)
        }
        None => bail!("No captures found"),
    }
}

fn load_point(dir: &Path) -> Result<BTreeMap<String, i16>> {
    let mut temps = BTreeMap::new();
    for entry in std::fs::read_dir(dir)? {
        let entry = entry?;
        let path = entry.path();
        let filename_raw = path.file_name().with_context(|| {
            format!("Couldn't get file name of {:?}", entry.path())
        })?;
        let filename = filename_raw.as_str().with_context(|| {
            format!(
                "Couldn't get file name, possibly \
                            invalid UTF-8: {:?}",
                filename_raw
            )
        })?;
        temps.insert(filename.to_string(), read_file(&entry.path())?);
    }
    Ok(temps)
}

fn main() -> Result<()> {
    let opt = Opt::from_args();

    let config: Configuration = if let Some(path) = opt.configfile {
        let config_str = std::fs::read_to_string(&path)?;
        toml::from_str(&config_str)?
    } else {
        Default::default()
    };

    opt.command.process(&config)?;

    Ok(())
}

fn write_chart(
    chartpath: &Path,
    width: u32,
    height: u32,
    start: DateTime<Local>,
    end: DateTime<Local>,
    data: &ChartData,
) -> Result<()> {
    let root = BitMapBackend::new(&chartpath, (width, height))
        .into_drawing_area();
    root.fill(&WHITE)?;

    let mut lines = BTreeMap::new();
    let mut min = -OrderedFloat::<f32>::one();
    let mut max = OrderedFloat::<f32>::one();

    for (k, line) in data.into_iter() {
        let line = line
            .into_iter()
            .filter(|(date, _)| {
                **date
                    > start
                        .checked_sub_signed(Duration::minutes(11))
                        .unwrap()
                    && **date < end
            })
            .map(|(date, value)| {
                max = std::cmp::max(
                    max,
                    *value + OrderedFloat::<f32>::one(),
                );
                min = std::cmp::min(
                    min,
                    *value - OrderedFloat::<f32>::one(),
                );
                (*date, *value)
            })
            .collect::<BTreeMap<DateTime<Local>, OrderedFloat<f32>>>();
        lines.insert(k, line);
    }

    let mut chart = ChartBuilder::on(&root)
        .margin(5)
        .x_label_area_size(30)
        .top_x_label_area_size(60)
        .y_label_area_size(30)
        .right_y_label_area_size(60)
        .build_cartesian_2d(
            start..end,
            min.into_inner()..max.into_inner(),
        )?;

    chart
        .configure_mesh()
        .x_label_formatter(&|dt| {
            dt.naive_local().format("%d.%m %H:%M").to_string()
        })
        .draw()?;
    chart.draw_series(LineSeries::new(
        vec![(start, 0f32), (end, 0f32)],
        ShapeStyle::from(&BLACK.mix(0.3)).stroke_width(2),
    ))?;

    let colors = vec![&BLUE, &GREEN, &MAGENTA, &RED, &CYAN];
    for (i, (k, curve)) in lines.iter().enumerate() {
        let color = colors[i % colors.len()];
        let style = ShapeStyle::from(color).stroke_width(2);
        chart
            .draw_series(LineSeries::new(
                curve.iter().map(|(k, v)| (*k, v.into_inner())),
                style.clone(),
            ))?
            .label(k.to_string())
            .legend(move |(x, y)| {
                PathElement::new(vec![(x, y), (x + 20, y)], style.clone())
            });
    }

    chart
        .configure_series_labels()
        .position(SeriesLabelPosition::LowerLeft)
        .background_style(&WHITE.mix(0.6))
        .border_style(&BLACK)
        .draw()?;

    Ok(())
}

trait PathAsStr {
    fn as_str(&self) -> Result<&str>;
}

impl<P: AsRef<Path>> PathAsStr for P {
    fn as_str(&self) -> Result<&str> {
        let p = self.as_ref();
        p.to_str().with_context(|| {
            format!("Couldn't get path, possibly invalid UTF-8: {:?}", p)
        })
    }
}
